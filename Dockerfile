FROM debian:bullseye

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get -y install python3-pip git gnupg2 && \
    python3 -m pip install ansible